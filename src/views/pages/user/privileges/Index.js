/*eslint-disable*/
import React, { useEffect, useState } from "react";
import {
  Card,
  Button,
  CardBody,
  CardHeader,
  UncontrolledTooltip,
  ButtonGroup,
} from "reactstrap";
import { Link } from "react-router-dom";
import axios from "axios";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider from "react-bootstrap-table2-toolkit";

const Privileges = () => {
  const token = localStorage.token;
  const headers = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
  };
  const redirectPrefix = `/admin/privileges/edit/`;
  const [rowIndex, setRowIndex] = useState(0);
  const [allPricelegesReturn, setAllPricelegesReturn] = useState([]);
  const [page, setPage] = useState(1);
  const [perPage, setPerpage] = useState(10);
  const [totalItem, setTotalItem] = useState(0);
  const [currentSort, setCurrentSort] = useState("");

  let paginationOption = {
    page: page,
    alwaysShowAllBtns: true,
    override: true,
    showTotal: true,
    withFirstAndLast: false,
    sizePerPage: perPage,
    totalSize: totalItem,
    onPageChange: (page) => {
      updateDataTable(page, perPage, currentSort);
    },
    sizePerPageRenderer: () => (
      <div className="dataTables_length" id="datatable-basic_length">
        <label>
          Show{" "}
          {
            <select
              name="datatable-basic_length"
              aria-controls="datatable-basic"
              className="form-control form-control-sm"
              onChange={(e) => {
                updateDataTable(page, e.target.value, currentSort);
              }}
            >
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="25">25</option>
              <option value="50">50</option>
            </select>
          }{" "}
          entries.
        </label>
      </div>
    ),
  };

  const updateDataTable = (page, perPage, sort) => {
    getPricelegesReturn(page, perPage, sort);
    setPage(page);
    setPerpage(perPage);
    setRowIndex((page - 1) * perPage);
    setCurrentSort(sort);
  };

  const handleTableChange = (type, { sortField, sortOrder }) => {
    if (type === "sort") {
      let sort = `${sortField} ${sortOrder}`;
      updateDataTable(page, perPage, sort);
    }
  };

  useEffect(() => {
    getPricelegesReturn(page, perPage, currentSort);
  }, [currentSort, page, perPage]);

  const getPricelegesReturn = (page, perPage, currentSort) => {
    let filter = { page: page, per_page: perPage };
    const data = filter;
    axios
      .post(`${process.env.REACT_APP_API_BASE_URL}/users/privileges`, data, {
        headers,
      })
      .then((data) => {
        setAllPricelegesReturn(data.data.response);
        setPage(data.data.current_page + 1);
        setPerpage(data.data.per_page);
        setTotalItem(data.data.total_item);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const rowEvents = {
    onDoubleClick: (e, row, rowIndex) => {
      // setHide(false);
      // setSelectedAcquirerId(row.acquirer_id);
      // setSelectAcquirerName(row.acquirer_name);
    },
  };

  return (
    <Card>
      <CardHeader>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <h3>List User Privileges</h3>
        </div>
      </CardHeader>
      <CardBody>
        <ToolkitProvider
          rowNumber={rowIndex}
          data={allPricelegesReturn}
          keyField="id"
          columns={[
            {
              dataField: "no",
              text: "#",
              sort: true,
              page: 1,
              formatter: (cell, row, index) => {
                let currentRow = ++index;
                return currentRow + rowIndex;
              },
            },
            {
              dataField: "username",
              text: "User Name",
              sort: true,
            },
            {
              dataField: "create",
              text: "Create",
              sort: true,
            },
            {
              dataField: "read",
              text: "Read",
              sort: true,
            },
            {
              dataField: "update",
              text: "Update",
              sort: true,
            },
            {
              dataField: "delete",
              text: "Delete",
              sort: true,
            },
            {
              dataField: "change_password",
              text: "Change_password",
              sort: true,
            },
            {
              dataField: "cetak",
              text: "Cetak",
              sort: true,
            },
            {
              dataField: "",
              text: "",
              formatter: (cell, row, index) => {
                return (
                  <ButtonGroup>
                    <Button>
                      <Link
                        to={{
                          pathname: redirectPrefix + row.id,
                          state: row,
                        }}
                        id={"tooltip_" + row.id}
                      >
                        <i className="fas fa-user-edit" />
                      </Link>
                    </Button>
                    <UncontrolledTooltip delay={0} target={"tooltip_" + row.id}>
                      Edit
                    </UncontrolledTooltip>
                  </ButtonGroup>
                );
              },
            },
          ]}
        >
          {(props) => (
            <div className="py-4 table-responsive">
              <BootstrapTable
                remote
                {...props.baseProps}
                bootstrap4={true}
                bordered={false}
                hover={true}
                rowEvents={rowEvents}
                pagination={paginationFactory({
                  ...paginationOption,
                })}
                onTableChange={handleTableChange}
              />
            </div>
          )}
        </ToolkitProvider>
      </CardBody>
    </Card>
  );
};

export default Privileges;
