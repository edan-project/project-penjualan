/*eslint-disable*/
import React, { useEffect, useState } from "react";
import {
  
  Card,
  Button,
  Row,
  Col,
  CardBody,
  CardHeader,
  CardFooter,
  Table,
  Container,
  Form,
  FormGroup,
  Label,
  Input,
  
} from "reactstrap";
import { Link,useHistory } from "react-router-dom";
import axios from "axios";
import SimpleHeader from "components/Headers/SimpleHeader.js";

export default function EditPoRetur(props)  {
  const token = localStorage.token;
  const username = localStorage.username;
  const warehouse = localStorage.warehouse;
  let history = useHistory();
  const [isLoading, setLoading] = useState(false);
  const [persons, setPersons] = useState([]);
  const [person, setPerson] = useState("");
  const [pocode, setPoCode] = useState("");
  const [keterangan, setKeterangan] = useState("");
  const [totalPrice, setTotalPrice] = useState(0);
  const [editingItem, setEditingitem] = useState([]);
  const [savedItems, setSavedItems] = useState([]);
  const [coderfq, setCodeRfq] = useState([]);
  const [type,setType] =useState([]);
  const [returtipe,setReturTipe] = useState([]);

  useEffect(() => {
    getById();
  }, []);

  const getById = () => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    axios
      .get(
        `${process.env.REACT_APP_API_BASE_URL}/po-retur/get/${props.match.params.id}`,
        { headers }
      )
      .then((data) => {
        getPerson(data.data.response.person_id);
        getItemDataSaved();
        setCodeRfq(data.data.response.code_rfq);
        setPoCode(data.data.po_code)
        setType(data.data.response.type);
        setReturTipe(data.data.response.return_type);
        setKeterangan(data.data.response.keterangan);
       
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const getItemDataSaved = () => {
    axios.post(`${process.env.REACT_APP_API_BASE_URL}/po-retur/item`, {

        rpo_id: props.match.params.id

    }).then(async response => {
        let stateItem = [];
        let stateEditing = [];

        await Promise.all(response.data.response.map(async (data) => {
            stateItem = [...stateItem, {
                item_id: data.item_id,
                item_name:data.item_name,
                qty: data.qty,
                harga: data.harga,
                diskon_nominal: data.diskon_nominal,
                diskon_persen: data.diskon_persen,
                data: {
                    item_name: data.item_name,
                    qty: data.qty,
                    harga: data.harga
                },
            }];
                stateEditing = [...stateEditing, {
                  editing: false
            }];
      }));
        setEditingitem(stateEditing);
        setSavedItems(stateItem);
    })
}

  const getPerson = (id) => {
      const headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      };
      axios
        .get(
          `${process.env.REACT_APP_API_BASE_URL}/person/list`,
          { headers }
        )
        .then((data) => {
          setPersons(data.data.response);
          setPerson(id);
        })
        .catch(function (error) {
          console.log(error);
        });
    };
  
  function EditData() {
    setLoading(true);
    let dataItems = [];
        savedItems.map((dataItem) => dataItems = [...dataItems, 
            { 
                item_id: dataItem.item_id, 
                qty: dataItem.qty, 
                harga: dataItem.harga,
                diskon_nominal: dataItem.diskon_nominal,
                diskon_persen:dataItem.diskon_persen,
            }]);
    let data = {
        warehouse_id : parseInt(warehouse),
        username : username,
        code_po: pocode,
        person_id: parseInt(person),
        keterangan: keterangan ,
        type: parseInt(type),
        status_d: 3,
        status_m: 3,
        return_type: parseInt(returtipe),
        items : dataItems,
    };
    axios
        .post(
          `${process.env.REACT_APP_API_BASE_URL}/po-retur/update/${props.match.params.id}`,
          data,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        )
        .then(function (response) {
          history.push("/admin/po-retur");
        })
        .then((json) => {
          setLoading(false);
        })
        .catch(function (error) {
          console.log(error);
        });
  }

const formatRupiah = (money) => {
  return new Intl.NumberFormat('id-ID',
      { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
  ).format(money);
}
  
  const handleSubmit = (e) => {
    e.preventDefault();
    {
      EditData();
    }
  };

  const deleteItem = (id) => {
    let array = [...savedItems];

    let index = array.findIndex(object => {
        return object.item_id === id;
    });

    if (index !== -1) {
        setTotalPrice(totalPrice - array[index].harga * array[index].qty);
        array.splice(index, 1);
        setSavedItems(array);
    }
}

const changePriceStatus = (index, status) => {
  setEditingitem([
      ...editingItem.slice(0, index),
      Object.assign({}, editingItem[index], { editing: status }),
      ...editingItem.slice(index + 1)
  ]);
}

const changeItemDataTable = async (arg) => {
    setTotalPrice(totalPrice - arg.defaultPrice + savedItems[arg.index].harga);

    setSavedItems([
        ...savedItems.slice(0, arg.index),
        Object.assign({}, savedItems[arg.index], {
            data: {
                item_name: arg.itemName,
                qty:savedItems[arg.index].qty,
                harga: savedItems[arg.index].harga
            }
        }),
        ...savedItems.slice(arg.index + 1)
    ]);

    changePriceStatus(arg.index, false);
}

  return (
    <>
    <SimpleHeader name="Edit Retur PO" parentName="PO" />
    <Container className="mt--6" fluid>
        <Row>
          <div className="col">
          <Form onSubmit={handleSubmit}>
              <Card>
                <CardBody>
                    <CardHeader>
                      <h3>Edit Retur Po</h3>
                    </CardHeader>
                    <CardBody>
                          <Col md="6">
                          <Input
                                  type="hidden"
                                  name="desc"
                                  placeholder="Masukan Keterangan"
                                  value={coderfq}
                                  onChange={(e) => {
                                    setCodeRfq(e.target.value);
                                  }}
                                />
                          </Col>
                          <Input
                                  type="hidden"
                                  name="desc"
                                  placeholder="Masukan Keterangan"
                                  value={pocode}
                                  onChange={(e) => {
                                    setPoCode(e.target.value);
                                  }}
                                />
                      <Row md="12">
                          <Col md="6">
                            <FormGroup row>
                              <Label
                                for="exampleEmail"
                                sm={4}
                              >
                                Supplier
                              </Label>
                              <Col sm={7}>
                                <Input
                                  name="person"
                                  type="select"
                                  value={person}
                                  onChange={(e) => {
                                    setPerson(e.target.value);
                                  }}
                                >
                                  <option value=''>Pilih Supplier</option>
                                  {persons.map((person, key) => {
                                      return (
                                        <option key={key} value={person.id}>
                                          {person.person_name}
                                        </option>
                                      );
                                    })}
                                </Input>
                              </Col>
                            </FormGroup>
                          </Col>
                          <Col md="6">
                          <FormGroup row>
                              <Label
                                for="exampleEmail"
                                sm={4}
                              >
                                Keterangan
                              </Label>
                              <Col sm={7}>
                                <Input
                                  type="text"
                                  name="desc"
                                  placeholder="Masukan Keterangan"
                                  value={keterangan}
                                  onChange={(e) => {
                                    setKeterangan(e.target.value);
                                  }}
                                />
                              </Col>                             
                            </FormGroup>
                          </Col>
                      </Row>
                      <Row md="12">
                          <Col md="6">
                          <FormGroup row>
                              <Label
                                for="exampleEmail"
                                sm={4}
                              >
                                Tipe
                              </Label>
                              <Col sm={7}>
                                <Input
                                  name="Tipe Po"
                                  type="select"
                                  value={type}
                                  onChange={(e) => {
                                      setType(e.target.value);
                                  }}
                                >
                                  <option value={""}>Pilih Tipe </option>
                                  <option value={1}>Dari Supplier ke Toko</option>
                                  <option value={2}>Dari Supplier ke Customer</option>
                                </Input>
                              </Col>                             
                          </FormGroup>
                          </Col>
                          <Col md="6">
                          <FormGroup row>
                              <Label
                                for="exampleEmail"
                                sm={4}
                              >
                                Tipe Retur
                              </Label>
                              <Col sm={7}>
                                <Input
                                  name="Tipe Po"
                                  type="select"
                                  value={returtipe}
                                  onChange={(e) => {
                                      setReturTipe(e.target.value);
                                  }}
                                >
                                 <option value={""}>Pilih Retur</option>
                                  <option value={1}>Ganti Barang</option>
                                  <option value={2}>Ganti Saldo</option>
                                </Input>
                              </Col>                             
                          </FormGroup>
                          </Col>
                      </Row>
                        <Col xs="12">
                            <hr />
                        </Col>
                        <Table>
                        <thead>
                        <tr>
                            <th>
                            Nama Item
                            </th>
                            <th>
                            Quantity
                            </th>
                            <th>
                            Harga
                            </th>
                            <th>
                            Diskon Persen
                            </th>
                            <th>
                            Diskon Nominal
                            </th>
                            <th>
                            Aksi
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                                {
                                    savedItems.map((savedItem, key) => {
                                        return (
                                            <tr key={key}>
                                                <td>{savedItem.data.item_name}</td>
                                                <td>
                                                    {editingItem[key].editing ? (
                                                        <Input
                                                            placeholder="qty Fisik"
                                                            type="number"
                                                            value={savedItems[key].qty}
                                                            onChange={(e) => {
                                                                setSavedItems([
                                                                    ...savedItems.slice(0, key),
                                                                    Object.assign({}, savedItems[key], { qty: e.target.value}),
                                                                    ...savedItems.slice(key + 1)
                                                                ]);
                                                            }}
                                                        />
                                                    ) : (
                                                            <>{savedItem.data.qty}</>
                                                        )}
                                                </td>
                                                <td>{formatRupiah(savedItem.data.harga)}</td>
                                                <td>{savedItem.diskon_persen}</td>
                                                <td>{formatRupiah(savedItem.diskon_nominal)}</td>
                                                <td>
                                                    {editingItem[key].editing ? (
                                                        <>
                                                            <Button color="warning" onClick={() => changeItemDataTable({
                                                                index: key,
                                                                itemName: savedItem.data.item_name,
                                                                qty : savedItem.data.qty,
                                                                harga: savedItem.data.harga,
                                                                diskon_nominal: savedItem.data.diskon_nominal,
                                                                diskon_persen: savedItem.data.diskon_persen

                                                            })}>Update</Button>
                                                            <Button color="danger" onClick={() => {
                                                                setSavedItems([
                                                                    ...savedItems.slice(0, key),
                                                                    Object.assign({}, savedItems[key], { qty: savedItem.data.qty}),
                                                                    ...savedItems.slice(key + 1)
                                                                ]);

                                                                changePriceStatus(key, false);
                                                            }}>Cancel</Button>
                                                        </>
                                                    ) : (
                                                            <>
                                                                <Button color="warning" onClick={() => changePriceStatus(key, true)}>Edit</Button>
                                                                <Button color="danger" onClick={() => deleteItem(savedItem.item_id)}>Delete</Button>
                                                            </>
                                                        )}
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                              </tbody>
                      </Table>
                    </CardBody>
                </CardBody>
                <CardFooter>
                      {!isLoading && (
                        <Button color="primary" type="submit" >Simpan</Button>
                      )}
                      {isLoading && (
                        <Button color="primary" disabled>
                          <i className="fas fa-spinner fa-spin"></i>
                          {""}
                          loading...
                        </Button>
                      )}
                      <Link className="btn btn-info" to="/admin/po-retur">
                        Kembali
                      </Link>
                </CardFooter>
              </Card>
              </Form>
          </div>
        </Row>
    </Container>  
    </>
  );
}