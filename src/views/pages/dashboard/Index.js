/*eslint-disable*/
import React, { useEffect, useState } from "react";
import {
  Card,
  CardBody,
  Label,
  FormGroup,
  Row,
  Input,
  Col,
  CardHeader,
  CardFooter,
  Container,
  Form,
  Button,
  CardImg,
  CardImgOverlay,
  CardTitle,
  CardText,
  ListGroupItem,
  ListGroup,
} from "reactstrap";

import SimpleHeader from "components/Headers/SimpleHeader.js";


export default function dashboard() {
  
  
  return (
    <div> 
      <SimpleHeader name="Dashboard" parentName="Admin" />
      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardBody>
                
                  <Row className="mb-4">
                    <Col lg="4">
                      <Card className="bg-gradient-red">
                        <CardBody>
                          <Row>
                            <div className="col">
                              <CardTitle className="text-uppercase text-muted mb-0 text-white">
                                <b>Total Karyawan</b>
                              </CardTitle>
                              <span className="h2 font-weight-bold mb-0 text-white">
                                40 Karyawan
                              </span>
                            </div>
                            <Col className="col-auto">
                              <div className="icon icon-shape bg-white text-dark rounded-circle shadow">
                                <i className="ni ni-money-coins" />
                              </div>
                            </Col>
                          </Row>
                          <p className="mt-3 mb-0 text-sm">
                            <span className="text-white mr-2">
                              <i className="fa fa-arrow-up" />
                            </span>
                            <span className="text-nowrap text-light">
                            </span>
                          </p>
                        </CardBody>
                      </Card>
                    </Col>
                    <Col lg="4">
                      <Card className="bg-gradient-primary">

                        <CardBody>
                          <Row>
                            <div className="col">
                              <CardTitle className="text-uppercase text-muted mb-0 text-white">
                                Total Item
                              </CardTitle>
                              <span className="h2 font-weight-bold mb-0 text-white">
                                -
                              </span>
                            </div>
                            <Col className="col-auto">
                              <div className="icon icon-shape bg-white text-dark rounded-circle shadow">
                                <i className="ni ni-book-bookmark" />
                              </div>
                            </Col>
                          </Row>
                          <p className="mt-3 mb-0 text-sm">
                            <span className="text-white mr-2">
                              <i className="fa fa-arrow-up" />
                            </span>
                            <span className="text-nowrap text-light">
                            </span>
                          </p>
                        </CardBody>
                      </Card>
                    </Col>
                    <Col lg="4">
                      <Card className="bg-gradient-info">

                        <CardBody>
                          <Row>
                            <div className="col">
                              <CardTitle className="text-uppercase text-muted mb-0 text-white">
                                Total Cabang
                              </CardTitle>
                              <span className="h2 font-weight-bold mb-0 text-white">
                                -
                              </span>
                            </div>
                            <Col className="col-auto">
                              <div className="icon icon-shape bg-white text-dark rounded-circle shadow">
                                <i className="ni ni-shop" />
                              </div>
                            </Col>
                          </Row>
                          <p className="mt-3 mb-0 text-sm">
                            <span className="text-white mr-2">
                              <i className="fa fa-arrow-up" />
                            </span>
                            <span className="text-nowrap text-light">
                            </span>
                          </p>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
              </CardBody>
            </Card>
          </div>
        </Row>
        </Container>
    </div>
  );
}
