/*eslint-disable*/
import React, { useEffect, useState } from 'react';
import { 
    Card, 
    Button, 
    Row, 
    Col, 
    CardBody, 
    CardHeader,
    Container,
    ButtonGroup, 
    Form, 
    FormGroup, 
    Label, 
    Input 
} from 'reactstrap';
import { Link } from "react-router-dom";
import axios from 'axios';
import SimpleHeader from "components/Headers/SimpleHeader.js";

const SettlementChasier = () => {
  return (
    <div>
    <SimpleHeader name="Opening Cashsier " parentName="SO" />
      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardBody>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <center>
                <Col xs={6}>
                      <Link style={{
                        fontSize: 50,
                        paddingTop: 20,
                        top: "50%",
                        height: 117,
                        resize: "none",
                      }}  className="btn btn-info" to="/admin/kasir-sales-order/modal">
                    Open Cashsier
                    </Link>
                  </Col>
                 </center>
                 <br></br>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
              </CardBody>
            </Card>
          </div>
        </Row>
        </Container>
    </div>
  );
}

export default SettlementChasier;