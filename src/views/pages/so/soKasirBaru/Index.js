/*eslint-disable*/
import React, { useEffect, useState} from "react";
import {
  Card,
  CardBody,
  Row,
  Col,
  Input,
  Container,
  Form,
  Table,
  Button,
  Label,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { Link,useHistory } from "react-router-dom";
import axios from "axios";
import SimpleHeader from "components/Headers/SimpleHeader.js";

export default function CreateSalesOrder() {
  const token = localStorage.token;
  const warehouse = localStorage.warehouse;
  const username = localStorage.username;
  const name = localStorage.name;

  let history = useHistory();
  const [isOpen, setIsOpen] = useState(false);
  const [totalPrice, setTotalPrice] = useState(0);
  const redirectPrefix1 = `/admin/kasir-sales-order/detail`;
  const [barcode, setBarcode] = useState("");
  const [pengiriman, setPengiriman] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [customer, setCustomer] = useState("");
  const [payment_method1, setPaymentMethod1] = useState([]);
  const [payment_method2, setPaymentMethod2] = useState([]);
  const [keterangan1, setKeterangan1] = useState("");
  const [keterangan2, setKeterangan2] = useState("");
  const [savedItems, setSavedItems] = useState([]);
  const [qty, setQty] = useState(1);
  const [payment_method, setPaymentMethod] = useState([]);
  const [pay1, setPay1] = useState(0);
  const [pay2, setPay2] = useState(0);
  const [change, setChange] = useState(0);
  const [banks, setBanks] = useState([]);

  useEffect(() => {
    getCustomer();
    getBank();
  }, []);

  const getCustomer = () => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    axios
      .get(
        `${process.env.REACT_APP_API_BASE_URL}/customer/list`,
        { headers }
      )
      .then((data) => {
        setCustomers(data.data.response);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const getBank = () => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    axios
      .get(
        `${process.env.REACT_APP_API_BASE_URL}/bank/get-by-wh/${warehouse}`,
        { headers }
      )
      .then((data) => {
        setBanks(data.data.response);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onChange1 = (e) => {
    const barcode = e.currentTarget.value;
    axios.post(`${process.env.REACT_APP_API_BASE_URL}/items`, {
        page: 1,
        per_page: 1,
        barcode: barcode,
        warehouse_id : parseInt(warehouse),
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).then(async response => {

        let suggests = [];
        await Promise.all(response.data.response.map((data) =>
            suggests = [...suggests, data.barcode]
        ));
    });

    setBarcode(e.currentTarget.value);
};

const formatRupiah = (money) => {
    return new Intl.NumberFormat('id-ID',
        { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
    ).format(money);
  }
const saveItem = () => {
    axios.post(`${process.env.REACT_APP_API_BASE_URL}/items`, {
        page: 1,
        per_page: 1,
        barcode: barcode,
        warehouse_id : parseInt(warehouse)
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }).then(async response => {
        const length = response.data.response.length;
        if (length === 0)
            return;

        const idItem = response.data.response[0].id;
        const qtyy = 1;
        const headers = {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        };
        axios.get(`${process.env.REACT_APP_API_BASE_URL}/items-by-price?item_id=${idItem}&qty=${qtyy}
        `, {headers})
        .then(async response => {
            let stateItem = [];
            await Promise.all(response.data.response.map(async (data) => {
                stateItem = [...savedItems, {
                    item_id: idItem,
                    item_name:data.item_name,
                    qty: 1,
                    satuan:data.satuan,
                    harga: data.harga,
                    data: {
                        item_name: data.item_name,
                        harga: data.harga
                    },
                }];
                setTotalPrice(totalPrice + data.harga * parseInt(qty));
            }));
            setSavedItems(stateItem);
        })
      });
}

const handleSubmit = (e) => {
    e.preventDefault();
    {
      saveItem();
      setBarcode("");
    }
  };

  const deleteItem = (id) => {
    let array = [...savedItems];

    let index = array.findIndex(object => {
        return object.item_id === id;
    });

    if (index !== -1) {
        setTotalPrice(totalPrice - array[index].harga * array[index].qty);
        array.splice(index, 1);
        setSavedItems(array);
    }
}

  return (
    <>
      <SimpleHeader name="Cashier" parentName="SO" />
      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardBody>
                <Row>
                  <Col xs={6} className="mb-3">
                    <textarea
                      className="form-control"
                      disabled
                      style={{
                        fontSize: 50,
                        paddingTop: 20,
                        top: "50%",
                        height: 117,
                        resize: "none",
                      }}
                      value={
                        "Rp." +
                        totalPrice
                          .toFixed(2)
                          .replace(/\d(?=(\d{3})+\.)/g, "$&,") +
                        ",-"
                      }
                    ></textarea>
                  </Col>
                  <Col xs={3}>
                    <Label>
                        <b>SHIFT : "{name}"</b>
                    </Label>
                  </Col>
                </Row>
                <Form onSubmit={handleSubmit} id="form">
                <Row className="mb-3">
                  <Col xs={3}>
                    <div style={{ display: "flex" }}>
                      <Input
                          placeholder="Masukan Kode Barcode "
                          type="text"
                          onChange={onChange1}
                          value={barcode}
                      />
                      </div>
                  </Col>
                  <Button hidden className="mb-3" color="primary" type="submit ">
                  </Button>
                  <Col xs={3}>
                    <Input
                      name="Tipe Po"
                      type="select"
                      value={payment_method}
                      onChange={(e) => {
                          setPaymentMethod(e.target.value);
                      }}
                    >
                      <option value={""}>Jenis Transaksi</option>
                      <option value={1}>Tunai</option>
                      <option value={3}>Inden DP</option>
                      <option value={4}>Inden Lunas</option>
                    </Input>
                  </Col>
                  <Col xs={3}>
                    <Button block color="primary" onClick={() => setIsOpen(!isOpen)}>
                      Bayar
                    </Button>
                  </Col>
                  <Col xs={3}>
                    <Link className="btn btn-danger mb-3" to="/admin/kasir-sales-order/detail">
                    Closing Cashsier
                    </Link>
                  </Col>
                </Row>
                </Form>
                <Row className="mb-3">
                  <Col xs={3}>
                  <Input
                    name="customer"
                    type="select"
                    value={customer}
                    onChange={(e) => {
                      setCustomer(e.target.value);
                    }}
                  >
                    <option value=''>Pilih Customer</option>
                    {customers.map((customer, key) => {
                        return (
                          <option key={key} value={customer.id}>
                            {customer.name}
                          </option>
                        );
                      })}
                    </Input>
                  </Col>
                  <Col xs={3}>
                  <Input
                    name="Tipe Request"
                    type="select"
                    value={pengiriman}
                    onChange={(e) => {
                      setPengiriman(e.target.value);
                    }}
                  >
                    <option value="">Pilih Pengiriman</option>
                    <option value={1}>Ambil Sendiri</option>
                    <option value={2}>Delivery</option>
                    <option value={3}>Kurir</option>
                  </Input>
                  </Col>
                </Row>
                <Row className="mb-3">
                  <Col xs={3}>
                  </Col>
                  <Col xs={3}>
                  </Col>
                  <Col xs={3}>
                  </Col>
                </Row>
                <Table>
                    <thead>
                    <tr>
                        <th>
                        Nama Item
                        </th>
                        <th>
                        Price
                        </th>
                        <th>
                        Quantity
                        </th>
                        <th>
                          Sub Total
                        </th>
                        <th>
                        Aksi
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        savedItems.map((savedItem, key) => {
                            return (
                                <tr key={key}>
                                    <td>{savedItem.data.item_name}</td>
                                    <td>{formatRupiah(savedItem.data.harga)}</td>
                                    <td>{savedItem.qty}</td>
                                    <td>{formatRupiah(savedItem.data.harga * savedItem.qty)}</td>
                                    <td>
                                        <Button color="danger" onClick={() => deleteItem(savedItem.item_id)}>Hapus</Button>
                                    </td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </Table>
              </CardBody>
            </Card>
          </div>
        </Row>
      </Container>
      {/* modal pembayaran */}
      <Modal
        toggle={() => setIsOpen(!isOpen)}
        isOpen={isOpen}
        fade={false}
        style={{ minWidth: "70%", top: "-20%" }}
      >
        <ModalHeader toggle={() => setIsOpen(!isOpen)}>
          Metode Pembayaran
        </ModalHeader>
        <ModalBody
          cssModule={{
            alignText: "center",
          }}
        >
          <div className="container">
            <div className="row align-items-center mb-3">
              <div className="col-3">Nominal Pembayaran 1</div>
              <div className="col-1 text-center">:</div>
              <div className="col-4 text-center">
                <Input
                  type="number"
                  value={pay1}
                  placeholder="Masukan Nomminal Pembayaran"
                  onChange={(event) => {
                    console.log(
                      parseInt(change) - parseInt(event.target.value)
                    );
                    setChange(parseInt(change) - parseInt(event.target.value));
                    setPay1(event.target.value);
                  }}
                ></Input>
              </div>
              <div className="col-4 text-center">
                  <Input
                    name="Tipe Po"
                    type="select"
                    placeholder="Metode Pembayaran 1"
                    value={payment_method1}
                    onChange={(e) => {
                        setPaymentMethod1(e.target.value);
                    }}
                  >
                    <option value=''>Pilih Metode Pembayaran</option>
                    {banks.map((bank, key) => {
                        return (
                          <option key={key} value={bank.id}>
                            {bank.bank_name}
                          </option>
                        );
                      })}
                  </Input>
              </div>
            </div>
            <div className="row align-items-center mb-3">
              <div className="col-3">Keterangan 1</div>
              <div className="col-1 text-center">:</div>
              <div className="col-4 text-center">
              <Input
                      name="Tipe Po"
                      type="text"
                      placeholder="Keterangan 2"
                      value={keterangan1}
                      onChange={(e) => {
                          setKeterangan1(e.target.value);
                      }}
                    >
                    </Input>
              </div>
            </div>

            <div className="row align-items-center mb-3">
              <div className="col-3">Nominal Pembayaran 2</div>
              <div className="col-1 text-center">:</div>
              <div className="col-4 text-center">
                <Input
                  type="number"
                  value={pay2}
                  onChange={(event) => setPay2(event.target.value)}
                  placeholder="Masukan Nomminal Pembayaran"
                ></Input>
              </div>
              <div className="col-4 text-center">
              <Input
                      name="Tipe Po"
                      type="select"
                      placeholder="Metode Pembayaran 2"
                      value={payment_method2}
                      onChange={(e) => {
                          setPaymentMethod2(e.target.value);
                      }}
                    >
                      <option value=''>Pilih Metode Pembayaran</option>
                        {banks.map((bank, key) => {
                            return (
                              <option key={key} value={bank.id}>
                                {bank.bank_name}
                              </option>
                            );
                          })}
                    </Input>
              </div>
            </div>
            <div className="row align-items-center mb-3">
              <div className="col-3">Keterangan 2</div>
              <div className="col-1 text-center">:</div>
              <div className="col-4 text-center">
              <Input
                      name="Tipe Po"
                      type="text"
                      placeholder="Keterangan 2"
                      value={keterangan2}
                      onChange={(e) => {
                          setKeterangan2(e.target.value);
                      }}
                    >
                    </Input>
              </div>
            </div> 
            <div className="row align-items-center mb-3">
              <div className="col-3 text-start  display-1">Total</div>
              <div className="col-1 text-center">:</div>
              <div className="col-6 text-center">
                <textarea
                  className="form-control"
                  disabled
                  value={
                    "Rp." +
                    totalPrice.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,") +
                    ",-"
                  }
                  style={{
                    fontSize: 40,
                    paddingTop: 20,
                    top: "50%",
                    height: 95,
                    resize: "none",
                  }}
                ></textarea>
              </div>
            </div>
            <div className="row align-items-center mb-3">
              <div className="col-3  display-3">Kembali</div>
              <div className="col-1 text-center">:</div>
              <div className="col-6 text-center">
                <textarea
                  className="form-control"
                  disabled
                  value={
                    "Rp." +
                    (
                      -1 * parseInt(totalPrice) +
                      parseInt(pay1) +
                      parseInt(pay2)
                    )
                      .toFixed(2)
                      .replace(/\d(?=(\d{3})+\.)/g, "$&,") +
                    ",-"
                  }
                  style={{
                    fontSize: 40,
                    paddingTop: 20,
                    top: "50%",
                    height: 95,
                    resize: "none",
                  }}
                ></textarea>
              </div>
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button
            color="danger"
            outline
            onClick={() => {
              if (
                -1 * parseInt(totalPrice) +
                  parseInt(pay1) +
                  parseInt(pay2) <
                0
              ) {
                alert("Nominal Belum Mencukupi");
              } else {
                let dataItems = [];
                    savedItems.map((dataItem) => dataItems = [...dataItems, 
                        { 
                            item_id: dataItem.item_id, 
                            qty: dataItem.qty, 
                            harga: dataItem.harga,
                        }]);
                let data = {
                  warehouse_id: parseInt(warehouse),
                  username : username,
                  code_rfq:"",
                  sales:"",
                  customer_id: parseInt(customer),
                  pengiriman: parseInt(pengiriman),
                  payment_method: parseInt(payment_method),
                  jangka_waktu: "",
                  keterangan:"",
                  pay_1:parseInt(pay1),
                  payment_method1:parseInt(payment_method1),
                  keterangan1: keterangan1,
                  pay_2: parseInt(pay2),
                  payment_method2: parseInt(payment_method2),
                  keterangan2: keterangan2,
                  pay_3:0,
                  payment_method3:1,
                  keterangan3:"0",
                  pay_4:0,
                  payment_method4:1,
                  keterangan4:"0",
                  pay_5:0,
                  payment_method5:1,
                  keterangan5:"0",
                  pay_6:0,
                  payment_method6:1,
                  keterangan6:"0",
                  items : dataItems
                };
                axios
                    .post(
                      `${process.env.REACT_APP_API_BASE_URL}/sales-order/cashier/save`,
                      data,
                      {
                        headers: {
                          Authorization: `Bearer ${token}`,
                        },
                      }
                    )
                    .then(function (response) {
                      window.location.reload("/admin/kasir-sales-order/so-kasir");
                    })
                    .then((json) => {
                      setLoading(false);
                    })
                    .catch(function (error) {
                      console.log(error);
                    });
              }
            }}
          >
            Konfirmasi Pembayaran
          </Button>{" "}
          <Button onClick={() => setIsOpen(!isOpen)}>Cancel</Button>
        </ModalFooter>
      </Modal>
    </>
  );
}
