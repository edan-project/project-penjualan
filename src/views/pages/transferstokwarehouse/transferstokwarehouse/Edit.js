/*eslint-disable*/
import React, { useEffect, useState } from "react";
import {
  
  Card,
  Button,
  Row,
  Col,
  CardBody,
  CardHeader,
  CardFooter,
  Container,
  Form,
  FormGroup,
  Label,
  Input,
  Table,
  
} from "reactstrap";
import { Link,useHistory } from "react-router-dom";
import axios from "axios";
import SimpleHeader from "components/Headers/SimpleHeader.js";

export default function EditTransferStokWarehouse(props)  {
  const token = localStorage.token;
  const username = localStorage.username;
  const warehouse = localStorage.warehouse;
  let history = useHistory();
  const [isLoading, setLoading] = useState(false);
  const [warehouses1, setWarehouses1] = useState([]);
  const [warehouse1, setWarehouse1] = useState("");
  const [warehouses2, setWarehouses2] = useState([]);
  const [warehouse2, setWarehouse2] = useState("");
  const [pengiriman, setPengiriman] = useState([]);
  const [keterangan, setKeterangan] = useState("");
  const [isShow, setIsShow] = useState(false);
  const [active, setActive] = useState(0);
  const [filtered, setFiltered] = useState([]);
  const [input, setInput] = useState("");
  const [savedItems, setSavedItems] = useState([]);
  const [qty, setQty] = useState([]);
  const [ongkir, setOngkir] = useState(0);
  const [lainnya, setLainnya] = useState(0);

  useEffect(() => {
    getById();
  }, []);

  const getById = () => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    axios
      .get(
        `${process.env.REACT_APP_API_BASE_URL}/transfer-warehouse/get/${props.match.params.id}`,
        { headers }
      )
      .then((data) => {
        getWarehouse1(data.data.response.warehouse_id1);
        getWarehouse2(data.data.response.warehouse_id2);
        setPengiriman(data.data.response.pengiriman);
        setOngkir(data.data.response.ongkir);
        setLainnya(data.data.response.lainnya);
        setKeterangan(data.data.response.keterangan);
        getItemDataSaved();
       
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const getItemDataSaved = () => {
    axios.post(`${process.env.REACT_APP_API_BASE_URL}/transfer-warehouse/item`, {

        tw_id: props.match.params.id

    }).then(async response => {
        let stateItem = [];

        await Promise.all(response.data.response.map(async (data) => {
            stateItem = [...stateItem, {
                item_id: data.item_id,
                item_name:data.item_name,
                qty: data.qty,
                data: {
                    item_name: data.item_name,
                    harga: data.harga
                },
            }];
        }));

        setSavedItems(stateItem);
    })
}

  const getWarehouse2 = (id) => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    axios
      .get(
        `${process.env.REACT_APP_API_BASE_URL}/warehouse/list/all`,
        { headers }
      )
      .then((data) => {
        setWarehouses2(data.data.response);
        setWarehouse2(id)
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const getWarehouse1 = (id) => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    axios
      .get(
        `${process.env.REACT_APP_API_BASE_URL}/warehouse/list/all`,
        { headers }
      )
      .then((data) => {
        setWarehouses1(data.data.response);
        setWarehouse1(id);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onChange = (e) => {
    const input = e.currentTarget.value;

    axios.post(`${process.env.REACT_APP_API_BASE_URL}/items`, {
        page: 1,
        per_page: 10,
        item_name: input,
        warehouse_id: parseInt(warehouse)
    }).then(async response => {
        let suggests = [];

        await Promise.all(response.data.response.map((data) =>
            suggests = [...suggests, data.item_name]
        ));

        setActive(0);
        setFiltered(suggests);
        setIsShow(true);
    });

    setInput(e.currentTarget.value);
};
const onClick = e => {
    setActive(0);
    setFiltered([]);
    setIsShow(false);
    setInput(e.currentTarget.innerText)
};
const onKeyDown = e => {
    if (e.keyCode === 13) { // enter key
        setActive(0);
        setIsShow(false);
        setInput(filtered[active])
    }
    else if (e.keyCode === 38) { // up arrow
        return (active === 0) ? null : setActive(active - 1);
    }
    else if (e.keyCode === 40) { // down arrow
        return (active - 1 === filtered.length) ? null : setActive(active + 1);
    }
};

const AutoCompleTes = () => {
    if (isShow && input) {
        if (filtered.length) {
            return (
                <ul className="autocomplete">
                    {filtered.map((suggestion, index) => {
                        let className;
                        if (index === active) {
                            className = "active";
                        }
                        return (
                            <li key={index} className={className} onClick={onClick}>
                                {suggestion}
                            </li>
                        );
                    })}
                </ul>
            );
        } else {
            return (
                <div className="no-autocomplete">
                    <em>Not found</em>
                </div>
            );
        }
    }
    return <></>;
}

const saveItem = () => {
  if (qty === '' || qty <= 0)
      return;

  axios.post(`${process.env.REACT_APP_API_BASE_URL}/items`, {
      page: 1,
      per_page: 1,
      item_name: input
  }).then(async response => {
      const length = response.data.response.length;
      if (length === 0)
          return;

      const idItem = response.data.response[0].id;

      axios.get(`${process.env.REACT_APP_API_BASE_URL}/items/${idItem}`)
          .then(async response => {
              return {
                  item: response.data.response.items,
              };
          }).then((data) => {
              let stateItem = [...savedItems, {
                  item_id: idItem,
                  qty: parseInt(qty),
                  data: data.item,
              }];

              setSavedItems(stateItem);
          });
  });
}

  function EditData() {
    setLoading(true);
    let dataItems = [];
    savedItems.map((dataItem) => dataItems = [...dataItems, 
        { 
            item_id: dataItem.item_id, 
            qty: dataItem.qty,
        }]);
    let data = {
      warehouse_id : parseInt(warehouse),
      username : username,
      warehouse_id1: parseInt(warehouse1),
      warehouse_id2: parseInt(warehouse2),
      pengiriman: parseInt(warehouse),
      ongkir : parseFloat(ongkir),
      lainnya : parseFloat(lainnya),
      status_m: 3,
      status_d: 3,
      keterangan,
      items : dataItems
    };
    axios
        .post(
          `${process.env.REACT_APP_API_BASE_URL}/transfer-warehouse/update/${props.match.params.id}`,
          data,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        )
        .then(function (response) {
          history.push("/admin/transfer-warehouse-stock");
        })
        .then((json) => {
          setLoading(false);
        })
        .catch(function (error) {
          console.log(error);
        });
  }
  
  const handleSubmit = (e) => {
    e.preventDefault();
    {
      EditData();
    }
  };

 
  return (
    <>
    <SimpleHeader name="Edit Transfer Stok Warehouse" parentName="Inventori" />
    <Container className="mt--6" fluid>
        <Row>
          <div className="col">
              <Card>
                <Form onSubmit={handleSubmit}>
                <CardBody>
                    <CardHeader>
                      <h3>Edit Transfer Stok Warehouse</h3>
                    </CardHeader>
                    <CardBody>
                      <Row md="12">
                          <Col md="6">
                            <FormGroup row>
                              <Label
                                for="exampleEmail"
                                sm={4}
                              >
                                Warehouse 
                              </Label>
                              <Col sm={7}>
                                <Input
                                  name="person"
                                  type="select"
                                  value={warehouse1}
                                  onChange={(e) => {
                                    setWarehouse1(e.target.value);
                                  }}
                                >
                                  <option value=''>Pilih Warehouse</option>
                                  {warehouses1.map((warehouse, key) => {
                                      return (
                                        <option key={key} value={warehouse.id}>
                                          {warehouse.name}
                                        </option>
                                      );
                                    })}
                                </Input>
                              </Col>
                            </FormGroup>
                            <FormGroup row>
                              <Label
                                for="exampleEmail"
                                sm={4}
                              >
                                Keterangan
                              </Label>
                              <Col sm={7}>
                                <Input
                                  type="textarea"
                                  name="desc"
                                  placeholder="Masukan Keterangan"
                                  value={keterangan}
                                  onChange={(e) => {
                                    setKeterangan(e.target.value);
                                  }}
                                />
                              </Col>                             
                            </FormGroup>
                            <FormGroup row>
                              <Label
                                for="exampleEmail"
                                sm={4}
                              >
                                Ongkir
                              </Label>
                              <Col sm={7}>
                                <Input
                                  type="text"
                                  name="desc"
                                  placeholder="Masukan Keterangan"
                                  value={ongkir}
                                  onChange={(e) => {
                                    setOngkir(e.target.value);
                                  }}
                                />
                              </Col>                             
                            </FormGroup>
                          </Col>
                          <Col md="6">
                          <FormGroup row>
                              <Label
                                for="exampleEmail"
                                sm={4}
                              >
                                Warehouse 2
                              </Label>
                              <Col sm={7}>
                                <Input
                                  name="person"
                                  type="select"
                                  value={warehouse2}
                                  onChange={(e) => {
                                    setWarehouse2(e.target.value);
                                  }}
                                >
                                  <option value=''>Pilih Warehouse</option>
                                  {warehouses2.map((warehouse2, key) => {
                                      return (
                                        <option key={key} value={warehouse2.id}>
                                          {warehouse2.name}
                                        </option>
                                      );
                                    })}
                                </Input>
                              </Col>
                            </FormGroup>
                            <FormGroup row>
                            <Label
                              for="exampleEmail"
                              sm={4}
                            >
                              Pengiriman
                            </Label>
                            <Col sm={7}>
                            <Input
                                name="Tipe Request"
                                type="select"
                                value={pengiriman}
                                onChange={(e) => {
                                  setPengiriman(e.target.value);
                                }}
                              >
                                <option value="">Pilih Pengiriman</option>
                                <option value={1}>Ambil Sendiri</option>
                                <option value={2}>Delivery</option>
                              </Input>
                            </Col>
                            </FormGroup>
                            <FormGroup row>
                              <Label
                                for="exampleEmail"
                                sm={4}
                              >
                                Lain-lain
                              </Label>
                              <Col sm={7}>
                                <Input
                                  type="text"
                                  name="desc"
                                  placeholder="Masukan Keterangan"
                                  value={lainnya}
                                  onChange={(e) => {
                                    setLainnya(e.target.value);
                                  }}
                                />
                              </Col>                             
                            </FormGroup>
                          </Col>
                      </Row>
                      <br></br>
                      <br></br>
                      <br></br>
                      <Row md="12">
                          <Col md="6">
                            <FormGroup row>
                              <Label
                                  for="exampleEmail"
                                  sm={3}
                                >
                                  Item
                                </Label>
                               <Col sm={7}>
                              <Input
                                  placeholder="Item ..."
                                  type="text"
                                  style={{ height: 38 }}
                                  onChange={onChange}
                                  onKeyDown={onKeyDown}
                                  value={input}
                              />
                              </Col>
                              <AutoCompleTes />
                            </FormGroup>
                          </Col>
                          <Col md="6">
                          <FormGroup row>
                            <Label
                                  for="exampleEmail"
                                  sm={4}
                                >
                                  Quantity
                              </Label>
                            <Col sm={7}>
                              <Input
                                  placeholder="Quantity"
                                  type="number"
                                  style={{ height: 38 }}
                                  onChange={(e) => setQty(e.target.value)}
                              />
                            </Col>
                          </FormGroup>
                          </Col>
                      </Row>
                    <Col xl="12">
                        <Button color="primary" className="mb-3" onClick={() => saveItem()}>Add</Button>
                    </Col>
                      <Table>
                        <thead>
                        <tr>
                            <th>
                            Nama Item
                            </th>
                            <th>
                            Quantity
                            </th>
                            <th>
                            Aksi
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            savedItems.map((savedItem, key) => {
                                return (
                                    <tr key={key}>
                                        <td>{savedItem.data.item_name}</td>
                                        <td>{savedItem.qty}</td>
                                        <td><Button color="warning" onClick={() => changePriceStatus(key, true)}>Edit</Button></td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                      </Table>   
                    </CardBody>
                </CardBody>
                <CardFooter>
                      {!isLoading && (
                        <Button color="primary" type="submit">
                          Simpan
                        </Button>
                      )}
                      {isLoading && (
                        <Button color="primary" disabled>
                          <i className="fas fa-spinner fa-spin"></i>
                          {""}
                          loading...
                        </Button>
                      )}
                      <Link className="btn btn-info" to="/admin/transfer-warehouse-stock">
                        Kembali
                      </Link>
                </CardFooter>
                </Form>
              </Card>
          </div>
        </Row>
    </Container>
    </>
  );
}