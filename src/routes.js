
/*!

=========================================================
* Argon Dashboard PRO React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Login from "views/pages/login/Index.js";
import Dashboard from "views/pages/dashboard/Index.js";
import Users from "views/pages/user/Index.js";
import CreateUser from "views/pages/user/Create.js";
import EditUser from "views/pages/user/Edit.js";
import EditPrivileges from "views/pages/user/privileges/Edit.js";

import Alamat from "views/pages/address/Index.js";
import CreateAlamat from "views/pages/address/Create.js";
import EditAlamat from "views/pages/address/Edit.js";

import Account from "views/pages/account/Index.js";
import CreateAccount from "views/pages/account/Create.js";
import EditAccount from "views/pages/account/Edit.js";

import Bank from "views/pages/bank/Index.js";
import CreateBank from "views/pages/bank/Create";
import EditBank from "views/pages/bank/Edit";

import Asset from "views/pages/asset/Index.js";
import CreateAsset from "views/pages/asset/Create.js";
import EditAsset from "views/pages/asset/Edit.js";

import TypeAsset from "views/pages/asset/type/Index.js";
import CreateTypeAsset from "views/pages/asset/type/Create.js";
import EditTypeAsset from "views/pages/asset/type/Edit.js";

import Uom from "views/pages/satuan/Index.js";
import CreateUom from "views/pages/satuan/Create";
import EditUom from "views/pages/satuan/Edit.js";

import Customer from "views/pages/Customer/Index.js";
import CreateCustomer from "views/pages/Customer/Create.js";
import EditCustomer from "views/pages/Customer/Edit.js";

import Coa from "views/pages/coa/Index.js";
import CreateCoa from "views/pages/coa/Create.js";
import EditCoa from "views/pages/coa/Edit.js";

// import Reason from "views/pages/reason/Index.js";
// import CreateReason from "views/pages/reason/Create.js";

import Item from "views/pages/item/Index.js";
import CreateItem from "views/pages/item/itemMain/Create.js";
import EditItem from "views/pages/item/itemMain/Edit2.js";
import DetailItems from "views/pages/item/itemMain/detail/Index.js";

import ItemCategory from "views/pages/item/itemCategory/Index.js";
import CreateItemCategory from "views/pages/item/itemCategory/Create.js";
import EditItemCategory from "views/pages/item/itemCategory/Edit.js";

import Warehouse from "views/pages/warehouse/index.js";
import warehousepusat from "views/pages/warehouse/WarehousePusat/Index.js";
import CreateWarehousePusat from "views/pages/warehouse/WarehousePusat/Create.js";
import EditWarehousePusat from "views/pages/warehouse/WarehousePusat/Edit.js";

import warehousetoko from "views/pages/warehouse/WarehouseToko/Index.js";
import CreateWarehouseToko from "views/pages/warehouse/WarehouseToko/Create.js";
import EditWarehouseToko from "views/pages/warehouse/WarehouseToko/Edit.js";

import warehousegudang from "views/pages/warehouse/WarehouseGudang/Index.js";
import CreateWarehouseGudang from "views/pages/warehouse/WarehouseGudang/Create.js";
import EditWarehouseGudang from "views/pages/warehouse/WarehouseGudang/Edit.js";

import ItemSubCategory from "views/pages/item/itemSubCategory/Index.js";
import CreateItemSubCategory from "views/pages/item/itemSubCategory/Create.js";

import ItemFunction from "views/pages/item/itemFunction/Index.js";
import CreateItemFunction from "views/pages/item/itemFunction/Create.js";

import ItemSubFunction from "views/pages/item/itemSubFunction/Index.js";
import CreateItemSubFunction from "views/pages/item/itemSubFunction/Create.js";

import ItemMerek from "views/pages/item/itemMerek/Index.js";
import CreateItemMerek from "views/pages/item/itemMerek/Create.js";

import ItemSubMerek from "views/pages/item/itemSubMerek/Index.js";
import CreateItemSubMerek from "views/pages/item/itemSubMerek/Create.js";

import ItemGrade from "views/pages/item/itemGrade/Index.js";
import CreateItemGrade from "views/pages/item/itemGrade/Create.js";

import Person from "views/pages/person/Index.js";
import CreatePerson from "views/pages/person/Create.js";
import EditPerson from "views/pages/person/Edit.js";

import Pajak from "views/pages/pajak/Index.js";
import CreatePajak from "views/pages/pajak/Create.js";
import EditPajak from "views/pages/pajak/Edit.js";

import Rak from "views/pages/Rak/Index.js";
import CreateRak from "views/pages/Rak/Create";

import Po from "views/pages/po/Index.js";

import ValidasiPo from "views/pages/po/ValidasiPo/Index.js";
import EditValidasiPo from "views/pages/po/ValidasiPo/Edit.js";

import AdminApprove from "views/pages/po/AdminApprove/Index.js";
import EditAdminApprove from "views/pages/po/AdminApprove/Edit.js";

import ValidatorPo from "views/pages/po/ValidatorPo/Index.js";
import EditValidatorPo from "views/pages/po/ValidatorPo/Edit.js";
import CetakPO from "views/pages/po/CetakPo/Cetak";

import ReceivingPO from "views/pages/po/ReceivingPo/Index.js";
import CreateReceivingPO from "views/pages/po/ReceivingPo/ReceivingPo/Create.js";
import EditReceivingPO from "views/pages/po/ReceivingPo/ReceivingPo/Edit.js";
import DetailReceivingPO from "views/pages/po/ReceivingPo/ReceivingPo/Detail.js";
import CetakReceivingPo from "views/pages/po/ReceivingPo/CetakReceivingPo/Cetak";

import ValidasiReceivingPO from "views/pages/po/ReceivingPo/ValidasiReceivingPo/Edit.js";

import So from "views/pages/so/Index.js";

import ValidasiSo from "views/pages/so/ValidasiSo/Index.js";
import EditValidasiSo from "views/pages/so/ValidasiSo/Edit.js";

import AdminApproveSo from "views/pages/so/AdminApprove/Index.js";
import EditAdminApproveSo from "views/pages/so/AdminApprove/Edit.js";

import PimpinanSo from "views/pages/so/ValidatorSo/Index.js";
import EditPimpinanSo from "views/pages/so/ValidatorSo/Edit.js";

import CetakSo from "views/pages/so/CetakSo/Cetak";

import CreateSo from "views/pages/so/Create.js";
import EditSo from "views/pages/so/Edit.js";

// import SoKasir from "views/pages/so/soKasir/Create.js";
import SoKasir from "views/pages/so/soKasirBaru/Index.js";

import KasirSettlement from "views/pages/so/Kasir/Index.js";
import CreateKasirSettlement from "views/pages/so/Kasir/Create.js";
import DetailKasirSettlement from "views/pages/so/Kasir/Detail.js";

import Adjustment from "views/pages/adjustment/Index.js";
import CreateAdjustment from "views/pages/adjustment/Adjustment/Create.js";
import EditAdjustment from "views/pages/adjustment/Adjustment/Edit.js";
import ValidasiAdjustment from "views/pages/adjustment/ValidasiDirekturAdj/Edit.js";
import CetakAdjustment from "views/pages/adjustment/CetakAdjustment/Cetak";

import Promo from "views/pages/promo/Index.js";
import CreatePromo from "views/pages/promo/Create.js";
import EditPromo from "views/pages/promo/Edit.js";

import ItemStock from "views/pages/itemStock/Index.js";

import TransferStockWarehouse from "views/pages/transferstokwarehouse/Index.js";
import CreateTransferStockWarehouse from "views/pages/transferstokwarehouse/transferstokwarehouse/Create.js";
import EditTransferStockWarehouse from "views/pages/transferstokwarehouse/transferstokwarehouse/Edit.js";
import DetailTransferStockWarehouse from "views/pages/transferstokwarehouse/CetakTranferStokWarehouse/Detail.js";
import ValidasiKepalaTokoTSW from "views/pages/transferstokwarehouse/ValidasiKepalaToko/Edit.js";
import ValidasiDirekturTSW from "views/pages/transferstokwarehouse/ValidasiDirektur/Edit";

import MainJangka  from "views/pages/jangkaWaktu/Index";
import CreateJangkaWaktu from "views/pages/jangkaWaktu/Create";
import EditJangkaWaktu from "views/pages/jangkaWaktu/Edit";

import PermintaanBarang from "views/pages/po/PermintaanBarang/Index.js";
import CreatePermintaanBarang from "views/pages/po/PermintaanBarang/PermintaanBarang/Create.js"
import EditPermintaanBarang from "views/pages/po/PermintaanBarang/PermintaanBarang/Edit.js";
import ValidasiPermintaanBarang from "views/pages/po/PermintaanBarang/ValidasiPermintaanBarang/Index.js";
import EditValidasiPermintaanBarang from "views/pages/po/PermintaanBarang/ValidasiPermintaanBarang/Edit.js";
import CetakPermintaanBarang from "views/pages/po/PermintaanBarang/CetakPermintaanBarang/Cetak";

import CreatePenawaranPo from "views/pages/po/PenawaranPo/Create.js";
import EditPenawaranPo from "views/pages/po/PenawaranPo/Edit.js";

import MemoKurirPO from "views/pages/po/MemoKurirPo/Index.js";
import CreateMemoKurirPO from "views/pages/po/MemoKurirPo/MemoKurirPo/Create";
import DetailMemoKurirPO from "views/pages/po/MemoKurirPo/MemoKurirPo/Detail";
import ValidasiAdminPoo from "views/pages/po/MemoKurirPo/ValidasiAdminPo/Edit";
import CetakMemoPo from "views/pages/po/MemoKurirPo/CetakMemo/Cetak";

import InvoicePO from "views/pages/po/InvoicePo/Index.js";
import CreateInvoicePO from "views/pages/po/InvoicePo/InvoicePo/Create.js";
import EditInvoicePO from "views/pages/po/InvoicePo/InvoicePo/Edit.js";
import CetakInvoicePO from "views/pages/po/InvoicePo/CetakInvoice/Cetak";

import ValidasiAdminFinance from "views/pages/po/InvoicePo/ValidasiAdminFinance/Edit.js";
import ValidasiPimpinan from "views/pages/po/InvoicePo/ValidasiPimpinan/Edit.js";

import BuktiKasKeluar from "views/pages/po/BuktiKasKeluar/Index.js";
import CreateBuktiKasKeluar from "views/pages/po/BuktiKasKeluar/BuktiKasKeluar/Create.js";
import DetailBuktiKasKeluar from "views/pages/po/BuktiKasKeluar/BuktiKasKeluar/Detail.js";
import ValidasiAdminFinanceBkk from "views/pages/po/BuktiKasKeluar/ValidasiKepalaFinance/Edit.js";
import ValidasiDirekturBkk from "views/pages/po/BuktiKasKeluar/ValidasiPimpinan/Edit.js";

import BuktiKasMasuk from "views/pages/so/BuktiKasMasuk/Index.js";
import CreateBuktiKasMasuk from "views/pages/so/BuktiKasMasuk/BuktiKasMasuk/Create.js";
import DetailBuktiKasMasuk from "views/pages/so/BuktiKasMasuk/BuktiKasMasuk/Detail.js";
import ValidasiAdminFinanceBkm from "views/pages/so/BuktiKasMasuk/ValidasiAdminFinance/Edit.js";
import ValidasiDirekturBkm from "views/pages/so/BuktiKasMasuk/ValidasiDirekturBKM/Edit.js";

import PermintaanBarangSo from "views/pages/so/PermintaanBarang/Index.js";
import CreatePermintaanBarangSo from "views/pages/so/PermintaanBarang/PermintaanBarang/Create.js";
import DetailPermintaanBarangSo from "views/pages/so/PermintaanBarang/PermintaanBarang/Detail.js";

import EditPermintaanBarangSo from "views/pages/so/PermintaanBarang/PermintaanBarang/Edit.js";
import ValidasiPermintaanBarangSo from "views/pages/so/PermintaanBarang/ValidasiPermintaanBarang/Index.js";
import EditValidasiPermintaanBarangSo from "views/pages/so/PermintaanBarang/ValidasiPermintaanBarang/Edit.js";

import CreatePenawaranSo from "views/pages/so/PenawaranSo/Create.js"
import EditPenawaranSo from "views/pages/so/PenawaranSo/Edit.js";
import DetailPenawaranSo from "views/pages/so/PenawaranSo/Detail.js";

import SuratJalanSo from "views/pages/so/SuratJalanSo/Index.js";
import CreateSuratJalanSo from "views/pages/so/SuratJalanSo/SuratJalanSo/Create.js";
import EditSuratJalanSo from "views/pages/so/SuratJalanSo/SuratJalanSo/Edit.js";
import DetailSuratJalanSo from "views/pages/so/SuratJalanSo/SuratJalanSo/Detail.js";
import ValidasiSuratJalanSo from "views/pages/so/SuratJalanSo/ValidasiSuratJalanSo/Edit.js";
import CetakSuratJalan from "views/pages/so/SuratJalanSo/CetakSuratJalan/Cetak.js";

import InvoiceSo from "views/pages/so/InvoiceSo/Index.js";
import CreateInvoiceSo from "views/pages/so/InvoiceSo/InvoiceSo/Create.js"
import EditInvoiceSo from "views/pages/so/InvoiceSo/InvoiceSo/Edit.js"
import DetailInvoiceSo from "views/pages/so/InvoiceSo/InvoiceSo/Detail.js"
import Validasifinance from "views/pages/so/InvoiceSo/ValidasiInvoiceSo/Edit.js";
import ValidasiDirektur from "views/pages/so/InvoiceSo/ValidasiPemimpin/Edit.js";

import ReceivingTransfer from "views/pages/transferstokwarehouse/ReceivingTransfer/Index";
import CreateReceivingTransfer from "views/pages/transferstokwarehouse/ReceivingTransfer/ReceivingTransfer/Create";
import EditReceivingTransfer from "views/pages/transferstokwarehouse/ReceivingTransfer/ReceivingTransfer/Edit";
import ValidasiKepalaGudangR from "views/pages/transferstokwarehouse/ReceivingTransfer/ValidasiKepalaGudang/Edit";
import CetakReceivingTransfer from "views/pages/transferstokwarehouse/ReceivingTransfer/CetakReceivingTransfer/Cetak";

import SuratJalanTransfer from "views/pages/transferstokwarehouse/SuratJalanTransfer/Index";
import CreateTransferSuratJalan from "views/pages/transferstokwarehouse/SuratJalanTransfer/SuratJalanTransfer/Create";
import EditTransferSuratJalan from "views/pages/transferstokwarehouse/SuratJalanTransfer/SuratJalanTransfer/Edit";
import CetakSuratJalanTransfer from "views/pages/transferstokwarehouse/SuratJalanTransfer/CetakSuratJalan/Cetak";

import TransferRak from "views/pages/transferRak/Index";
import CreateTransferRak from "views/pages/transferRak/Create";

import DurasiOperasional from "views/pages/durasiOperasional/Index";
import CreateDurasiOperasional from "views/pages/durasiOperasional/Create";
import EditDurasiOperasional from "views/pages/durasiOperasional/Edit";

import PasswordOperasional from "views/pages/durasiPassword/Index";
import EditPasswordOperasional from "views/pages/durasiPassword/Edit";

import PoRetur from "views/pages/po/PoRetur/Index";
import CreatePoRetur from "views/pages/po/PoRetur/PoRetur/Create";
import EditPoRetur from "views/pages/po/PoRetur/PoRetur/Edit";
import ValidasiAdminTokoReturPo from "views/pages/po/PoRetur/ValidasiAdminToko/Edit";
import ValidasiDirekturPoRetur from "views/pages/po/PoRetur/ValidasiDirekturPoRetur/Edit";
import CetakReturPo from "views/pages/po/PoRetur/CetakPoRetur/Cetak";

import CetakInvoiceSO from "views/pages/so/InvoiceSo/CetakInvoice/Cetak";
import CetakBuktiKasKeluar from "views/pages/po/BuktiKasKeluar/CetakKasKeluar/Cetak";

import CetakPenawaranBarang from "views/pages/so/PermintaanBarang/CetakPenawaranBarang/Cetak";
import CetakBuktiKasMasuk from "views/pages/so/BuktiKasMasuk/CetakBuktiKasMasuk/Cetak";
import CetakTransferStokWarehouse from "views/pages/transferstokwarehouse/CetakTranferStokWarehouse/Cetak";

const routes = [
  {
    path: "/login",
    name: "Login",
    miniName: "L",
    component: Login,
    layout: "/auth",
    hidden: true,
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    miniName: "D",
    component: Dashboard,
    layout: "/admin",
    hidden: true,
  },
  {
    collapse: true,
    name: "Master",
    icon: "ni ni-folder-17 text-yellow",
    state: "MasterCollapse",
    roles: [
      "ROLE_SUPERADMIN",
      "ROLE_ADMIN",
      "ROLE_KARYAWAN",
      "ROLE_USER",
      "ROLE_OWNER",
    ],
    views: [
      {
        path: "/warehouse-gudang/edit/:id",
        name: "Edit Warehouse Pusat",
        miniName: "EWP",
        component: EditWarehouseGudang,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/warehouse-gudang/create",
        name: "Create Warehouse Pusat",
        miniName: "CWS",
        component: CreateWarehouseGudang,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/warehouse-gudang",
        name: "Warehouse Gudang",
        miniName: "WT",
        component: warehousegudang,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/warehouse-toko/edit/:id",
        name: "Edit Warehouse Toko",
        miniName: "EWT",
        component: EditWarehouseToko,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/warehouse-toko/create",
        name: "Create Warehouse Toko",
        miniName: "CWT",
        component: CreateWarehouseToko,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/warehouse-toko",
        name: "Warehouse Toko",
        miniName: "WT",
        component: warehousetoko,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/warehouse-pusat/edit/:id",
        name: "Edit Warehouse Pusat",
        miniName: "EWP",
        component: EditWarehousePusat,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/warehouse-pusat/create",
        name: "Create Warehouse Pusat",
        miniName: "CWS",
        component: CreateWarehousePusat,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/warehouse-pusat",
        name: "Warehouse Pusat",
        miniName: "WT",
        component: warehousepusat,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/warehouse",
        name: "Cabang",
        miniName: "W",
        component: Warehouse,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/rak/create",
        name: "Create Rak",
        miniName: "CTS",
        component: CreateRak,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/rak",
        name: "Lokasi Barang",
        miniName: "R",
        component: Rak,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/durasi-operasional/edit/:id",
        name: "Edit Durasi Operasional",
        miniName: "CTS",
        component: EditDurasiOperasional,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/durasi-operasional/create",
        name: "Create Durasi Operasional",
        miniName: "CTS",
        component: CreateDurasiOperasional,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/durasi-operasional",
        name: "Batas Waktu",
        miniName: "OD",
        component: DurasiOperasional,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/password-operasional/edit/:id",
        name: "Edit Durasi Operasional",
        miniName: "CTS",
        component: EditPasswordOperasional,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/password-operasional",
        name: " Validasi Password",
        miniName: "OD",
        component: PasswordOperasional,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/user/edit/:id",
        name: "Edit User",
        miniName: "EU",
        component: EditUser,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/privileges/edit",
        name: "Edit Privileges",
        miniName: "EP",
        component: EditPrivileges,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/user/create",
        name: "Create User",
        miniName: "CU",
        component: CreateUser,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/user",
        name: "Users",
        miniName: "U",
        component: Users,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/alamat/edit/:id",
        name: "Edit Alamat",
        miniName: "EA",
        component: EditAlamat,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/alamat/create",
        name: "Create Address",
        miniName: "CA",
        component: CreateAlamat,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/alamat",
        name: "Alamat",
        miniName: "A",
        component: Alamat,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/person/edit/:id",
        name: "Edit Person",
        miniName: "EP",
        component: EditPerson,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/person/create",
        name: "Create Person",
        miniName: "CP",
        component: CreatePerson,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/person",
        name: "Supplier",
        miniName: "P",
        component: Person,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/satuan/edit/:id",
        name: "Edit Satuan",
        miniName: "ES",
        component: EditUom,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/satuan/create",
        name: "Create Satuan",
        miniName: "CS",
        component: CreateUom,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/satuan",
        name: "Satuan",
        miniName: "S",
        component: Uom,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-grade/create",
        name: "Create Item Grade",
        miniName: "CIG",
        component: CreateItemGrade,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-grade",
        name: "Item Grade",
        miniName: "IG",
        component: ItemGrade,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-sub-merek/create",
        name: "Create Item Sub Merek",
        miniName: "CISM",
        component: CreateItemSubMerek,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-sub-merek",
        name: "Item Sub Merek",
        miniName: "ISM",
        component: ItemSubMerek,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-merek/create",
        name: "Create Item Merek",
        miniName: "CIM",
        component: CreateItemMerek,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-merek",
        name: "Item Merek",
        miniName: "IM",
        component: ItemMerek,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-sub-function/create",
        name: "Create Item Sub Function",
        miniName: "CISK",
        component: CreateItemSubFunction,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-sub-function",
        name: "Item Sub Function",
        miniName: "ISF",
        component: ItemSubFunction,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-function/create",
        name: "Create Item Function",
        miniName: "CSK",
        component: CreateItemFunction,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-function",
        name: "Item Function",
        miniName: "IF",
        component: ItemFunction,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-sub-kategori/create",
        name: "Create Sub Kategori",
        miniName: "CSK",
        component: CreateItemSubCategory,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-sub-kategori",
        name: "Item Sub Kategori",
        miniName: "ISK",
        component: ItemSubCategory,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-kategori/edit/:id",
        name: "Edit Item Kategori",
        miniName: "EIK",
        component: EditItemCategory,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-kategori/create",
        name: "Create Kategori",
        miniName: "CK",
        component: CreateItemCategory,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item-kategori",
        name: "Item Kategori",
        miniName: "IK",
        component: ItemCategory,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item/edit/:id",
        name: "Edit Item",
        miniName: "EI",
        component: EditItem,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item/details/:id",
        name: "Detail Item",
        miniName: "DI",
        component: DetailItems,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item/create",
        name: "Create Item",
        miniName: "CI",
        component: CreateItem,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/item",
        name: "Item",
        miniName: "i",
        component: Item,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/promo/edit/:id",
        name: "Edit Promo",
        miniName: "EP",
        component: EditPromo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/promo/create",
        name: "Create Promo",
        miniName: "EP",
        component: CreatePromo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/promo",
        name: "Promo Toko",
        miniName: "P",
        component: Promo,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      
      {
        path: "/customer/edit/:id",
        name: "Edit Customer",
        miniName: "EC",
        component: EditCustomer,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/customer/create",
        name: "Create Customer",
        miniName: "CC",
        component: CreateCustomer,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/customer",
        name: "Customer",
        miniName: "C",
        component: Customer,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/pajak/edit/:id",
        name: "Edit Pajak",
        miniName: "EP",
        component: EditPajak,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/pajak/create",
        name: "Create Pajak",
        miniName: "CP",
        component: CreatePajak,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/pajak",
        name: "PPN",
        miniName: "P",
        component: Pajak,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/jangka-waktu/update/:id",
        name: "Update Jangka Waktu",
        miniName: "UJW",
        component: "",
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/jangka-waktu/edit/:id",
        name: "Edit Jangka Waktu",
        miniName: "CJW",
        component: EditJangkaWaktu,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/jangka-waktu/create",
        name: "Create Jangka Waktu",
        miniName: "CJW",
        component: CreateJangkaWaktu,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/jangka-waktu",
        name: "Jatuh Tempo",
        miniName: "JW",
        component: MainJangka,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },      
      {
        path: "/type-asset/edit/:id",
        name: "Edit Type Asset",
        miniName: "EA",
        component: EditTypeAsset,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/type-asset/create",
        name: "Create Type Asset",
        miniName: "CA",
        component: CreateTypeAsset,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/type-asset",
        name: "Type Asset",
        miniName: "A",
        component: TypeAsset,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/asset/edit/:id",
        name: "Edit Asset",
        miniName: "EA",
        component: EditAsset,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/asset/create",
        name: "Create Asset",
        miniName: "CA",
        component: CreateAsset,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/asset",
        name: "Asset",
        miniName: "A",
        component: Asset,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bank/edit/:id",
        name: "Edit Bank",
        miniName: "CA",
        component: EditBank,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bank/create",
        name: "Create Bank",
        miniName: "CA",
        component: CreateBank,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bank",
        name: "Bank",
        miniName: "A",
        component: Bank,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/account/edit/:id",
        name: "Edit Account",
        miniName: "EA",
        component: EditAccount,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/account/create",
        name: "Create Account",
        miniName: "CA",
        component: CreateAccount,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/account",
        name: "Account",
        miniName: "A",
        component: Account,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/coa/edit/:id",
        name: "Edit COA",
        miniName: "EC",
        component: EditCoa,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/coa/create",
        name: "Create COA",
        miniName: "CC",
        component: CreateCoa,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/coa",
        name: "Coa",
        miniName: "C",
        component: Coa,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      // {
      //   path: "/reason/create",
      //   name: "Alasan Create",
      //   miniName: "IS",
      //   component: CreateReason,
      //   layout: "/admin",
      //   hidden: true,
      //   roles: [
      //     "ROLE_SUPERADMIN",
      //     "ROLE_ADMIN",
      //     "ROLE_KARYAWAN",
      //     "ROLE_USER",
      //     "ROLE_OWNER",
      //   ],
      // },
      // {
      //   path: "/reason",
      //   name: "Alasan",
      //   miniName: "IS",
      //   component: Reason,
      //   layout: "/admin",
      //   roles: [
      //     "ROLE_SUPERADMIN",
      //     "ROLE_ADMIN",
      //     "ROLE_KARYAWAN",
      //     "ROLE_USER",
      //     "ROLE_OWNER",
      //   ],
      // },
    ],
  },
  {
    collapse: true,
    name: "Inventory",
    icon: "ni ni-archive-2 text-orange",
    state: "inventoryCollapse",
    roles: [
      "ROLE_SUPERADMIN",
      "ROLE_ADMIN",
      "ROLE_KARYAWAN",
      "ROLE_USER",
      "ROLE_OWNER",
    ],
    views: [
      {
        path: "/stock-item",
        name: "Stock",
        miniName: "IS",
        component: ItemStock,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/stock-adjustment/cetak/:id",
        name: "validasi Adjustment",
        miniName: "CTS",
        component: CetakAdjustment,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/stock-adjustment/validasi-direktur/:id",
        name: "validasi Adjustment",
        miniName: "CTS",
        component: ValidasiAdjustment,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/stock-adjustment/edit/:id",
        name: "Edit Adjustment",
        miniName: "CTS",
        component: EditAdjustment,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/stock-adjustment/create",
        name: "Create Adjustment",
        miniName: "CTS",
        component: CreateAdjustment,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/stock-adjustment",
        name: "Adjustment",
        miniName: "ASO",
        component: Adjustment,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-warehouse-stock/cetak/:id",
        name: "Create Transfer Stock Warehouse",
        miniName: "CTS",
        component: CetakTransferStokWarehouse,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-warehouse-stock/detail/:id",
        name: "Create Transfer Stock Warehouse",
        miniName: "CTS",
        component: DetailTransferStockWarehouse,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-warehouse-stock/validasi-direktur/validasi/:id",
        name: "Validasi Transfer Stok Warehouse Kepala Toko",
        miniName: "CTS",
        component: ValidasiDirekturTSW,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-warehouse-stock/validasi-kepala-toko/validasi/:id",
        name: "Validasi Transfer Stok Warehouse Kepala Toko",
        miniName: "CTS",
        component: ValidasiKepalaTokoTSW,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-warehouse-stock/edit/:id",
        name: "Create Transfer Stock Warehouse",
        miniName: "CTS",
        component: EditTransferStockWarehouse,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-warehouse-stock/create",
        name: "Create Transfer Stock Warehouse",
        miniName: "CTS",
        component: CreateTransferStockWarehouse,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-warehouse-stock",
        name: "Transfer Stok",
        miniName: "TSW",
        component: TransferStockWarehouse,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/receiving-transfer/cetak/:id",
        name: "Create Receiving Transfer",
        miniName: "TSW",
        component: CetakReceivingTransfer,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/receiving-transfer/validasi-kepala-gudang/validasi/:id",
        name: "Create Receiving Transfer",
        miniName: "TSW",
        component: ValidasiKepalaGudangR,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/receiving-transfer/edit/:id",
        name: "Create Receiving Transfer",
        miniName: "TSW",
        component: EditReceivingTransfer,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/receiving-transfer/create",
        name: "Create Receiving Transfer",
        miniName: "TSW",
        component: CreateReceivingTransfer,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/receiving-transfer",
        name: "Receiving Transfer Stok",
        miniName: "TSW",
        component: ReceivingTransfer,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-surat-jalan/cetak/:id",
        name: "Transfer Surat Jalan",
        miniName: "TSW",
        component: CetakSuratJalanTransfer,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-surat-jalan/edit/:id",
        name: "Transfer Surat Jalan",
        miniName: "TSW",
        component: EditTransferSuratJalan,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-surat-jalan/create",
        name: "Transfer Surat Jalan",
        miniName: "TSW",
        component: CreateTransferSuratJalan,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-surat-jalan",
        name: "Surat Jalan Transfer Stok",
        miniName: "TSW",
        component: SuratJalanTransfer,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-rak/create",
        name: "Rak Transfer",
        miniName: "TR",
        component: CreateTransferRak,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/transfer-rak",
        name: "Rak Transfer Stok",
        miniName: "TSW",
        component: TransferRak,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
    ],
  },
  {
    collapse: true,
    name: "Pembelian",
    icon: "ni ni-cart text-red",
    state: "POCollapse",
    roles: [
      "ROLE_SUPERADMIN",
      "ROLE_ADMIN",
      "ROLE_KARYAWAN",
      "ROLE_USER",
      "ROLE_OWNER",
    ],
    views: [
      {
        path: "/permintaan-barang/cetak/:id",
        name: "Edit Validasi Permintaan Barang",
        miniName: "CPB",
        component: CetakPermintaanBarang,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/validasi-permintaan-barang/edit/:id",
        name: "Edit Validasi Permintaan Barang",
        miniName: "CPB",
        component: EditValidasiPermintaanBarang,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/validasi-permintaan-barang",
        name: "Validasi Permintaan Barang",
        miniName: "CPB",
        component: ValidasiPermintaanBarang,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/permintaan-barang/create",
        name: "Create Permintaan Barang",
        miniName: "CPB",
        component: CreatePermintaanBarang,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/permintaan-barang/edit/:id",
        name: "Permintaan Barang Edit",
        miniName: "VPE",
        component: EditPermintaanBarang,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/permintaan-barang",
        name: "RFQ",
        miniName: "PB",
        component: PermintaanBarang,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/purchase-order/cetak/:id",
        name: "Cetak So",
        miniName: "EVPS",
        component: CetakPO,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/purchase-order/validasi-pimpinan-po/edit/:id",
        name: "Validasi Pimpinan Edit",
        miniName: "VPE",
        component: EditValidatorPo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/purchase-order/validasi-pimpinan-po",
        name: "Validasi Pimpinan",
        miniName: "VP",
        component: ValidatorPo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/purchase-order/validasi-admin-approve-po/edit/:id",
        name: "Validasi Penawaran Po",
        miniName: "VPP",
        component: EditAdminApprove,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/purchase-order/validasi-admin-approve-po",
        name: "Admin Approve",
        miniName: "AA",
        component: AdminApprove,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/purchase-order/validasi-penawaran-po/edit/:id",
        name: "Validasi Penawaran Po",
        miniName: "VPP",
        component: EditValidasiPo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/purchase-order/validasi-penawaran-po",
        name: "Validasi Penawaran Po",
        miniName: "VP",
        component: ValidasiPo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/purchase-order/edit/:id",
        name: "Edit Penawaran Po",
        miniName: "EPP",
        component: EditPenawaranPo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/purchase-order/create",
        name: "Create Penawaran Po",
        miniName: "CPP",
        component: CreatePenawaranPo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/purchase-order",
        name: "Order",
        miniName: "Po",
        component: Po,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/memo-kurir-po/cetak/:id",
        name: "Detail Penawaran Po",
        miniName: "DPP",
        component: CetakMemoPo,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/memo-kurir-po/validasi-admin-po/:id",
        name: "Detail Penawaran Po",
        miniName: "DPP",
        component: ValidasiAdminPoo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/memo-kurir-po/detail/:id",
        name: "Detail Penawaran Po",
        miniName: "DPP",
        component: DetailMemoKurirPO,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/memo-kurir-po/create",
        name: "Create Penawaran Po",
        miniName: "CPP",
        component: CreateMemoKurirPO,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/memo-kurir-po",
        name: "Memo Kurir",
        miniName: "MKP",
        component: MemoKurirPO,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/receiving-po/cetak/:id",
        name: "Validasi Receiving Po",
        miniName: "CPP",
        component: CetakReceivingPo,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/receiving-po/validasi/:id",
        name: "Validasi Receiving Po",
        miniName: "CPP",
        component: ValidasiReceivingPO,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/receiving-po/detail/:id",
        name: "Edit Receiving Po",
        miniName: "CPP",
        component: DetailReceivingPO,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/receiving-po/edit/:id",
        name: "Edit Receiving Po",
        miniName: "CPP",
        component: EditReceivingPO,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/receiving-po/create",
        name: "Create Receiving Po",
        miniName: "CPP",
        component: CreateReceivingPO,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/invoice-po/cetak/:id",
        name: "Edit Receiving Po",
        miniName: "CPP",
        component: CetakInvoicePO,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/receiving-po",
        name: "Receiving",
        miniName: "RP",
        component: ReceivingPO,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/invoice-po/validasi-direktur/validasi/:id",
        name: "Validasi Direktur Receiving Po",
        miniName: "CPP",
        component: ValidasiPimpinan,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/invoice-po/validasi-admin-finance/validasi/:id",
        name: "Validasi Admin Finance Receiving Po",
        miniName: "CPP",
        component: ValidasiAdminFinance,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/invoice-po/edit/:id",
        name: "Edit Receiving Po",
        miniName: "CPP",
        component: EditInvoicePO,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/invoice-po/create",
        name: "Create Invoice Po",
        miniName: "CPP",
        component: CreateInvoicePO,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/invoice-po",
        name: "Invoice",
        miniName: "IP",
        component: InvoicePO,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-keluar/cetak/:id",
        name: "Validasi Direktur",
        miniName: "CPP",
        component: CetakBuktiKasKeluar,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-keluar/validasi-direktur/validasi/:id",
        name: "Validasi Direktur",
        miniName: "CPP",
        component: ValidasiDirekturBkk,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-keluar/validasi-kepala-finance/validasi/:id",
        name: "Validasi kepala finance",
        miniName: "CPP",
        component: ValidasiAdminFinanceBkk,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-keluar/detail/:id",
        name: "Detail Bukti kas Keluar",
        miniName: "CPP",
        component: DetailBuktiKasKeluar,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-keluar/create",
        name: "Create bukti kas keluar",
        miniName: "CPP",
        component: CreateBuktiKasKeluar,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-keluar",
        name: "Bukti Kas Keluar",
        miniName: "BKK",
        component: BuktiKasKeluar,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/po-retur/cetak/:id",
        name: "Create Po Retur",
        miniName: "CPP",
        component: CetakReturPo,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/po-retur/validasi-direktur-po/:id",
        name: "Create Po Retur",
        miniName: "CPP",
        component: ValidasiDirekturPoRetur,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/po-retur/validasi-manajer-po/:id",
        name: "Create Po Retur",
        miniName: "CPP",
        component: ValidasiAdminTokoReturPo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/po-retur/edit/:id",
        name: "Create Po Retur",
        miniName: "CPP",
        component: EditPoRetur,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/po-retur/create",
        name: "Create Po Retur",
        miniName: "CPP",
        component: CreatePoRetur,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/po-retur",
        name: "PO Retur",
        miniName: "R",
        component: PoRetur,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
    ],
  },
  {
    collapse: true,
    name: "Penjualan",
    icon: "ni ni-cart text-red",
    state: "SOCollapse",
    roles: [
      "ROLE_SUPERADMIN",
      "ROLE_ADMIN",
      "ROLE_KARYAWAN",
      "ROLE_USER",
      "ROLE_OWNER",
    ],
    views: [
      {
        path: "/kasir-sales-order/detail",
        name: "Detail Kasir Settlement",
        miniName: "SO",
        component: DetailKasirSettlement,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/kasir-sales-order/so-kasir",
        name: "Kasir",
        miniName: "HSO",
        component: SoKasir,
        hidden: true,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/kasir-sales-order/modal",
        name: "Kasir Settlement",
        miniName: "SO",
        component: CreateKasirSettlement,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/kasir-sales-order/",
        name: "POS Kasir",
        miniName: "OK",
        component: KasirSettlement,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/so-permintaan-barang/cetak/:id",
        name: "Edit Validasi Pimpinan So",
        miniName: "EVPS",
        component: CetakPenawaranBarang,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/so-permintaan-barang/so-validasi-permintaan-barang/edit/:id",
        name: "Edit Validasi Pimpinan So",
        miniName: "EVPS",
        component: EditValidasiPermintaanBarangSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/so-permintaan-barang/so-validasi-permintaan-barang",
        name: " Validasi Pimpinan So",
        miniName: "EVPS",
        component: ValidasiPermintaanBarangSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/so-permintaan-barang/edit/:id",
        name: "Edit Validasi Pimpinan So",
        miniName: "EVPS",
        component: EditPermintaanBarangSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/so-permintaan-barang/detail/:id",
        name: "Detail Penawaran Barang",
        miniName: "SO",
        component: DetailPermintaanBarangSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/so-permintaan-barang/create",
        name: "Penawaran Barang",
        miniName: "SO",
        component: CreatePermintaanBarangSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/so-permintaan-barang",
        name: "Penawaran",
        miniName: "SO",
        component: PermintaanBarangSo,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/cetak/:id",
        name: "Cetak So",
        miniName: "EVPS",
        component: CetakSo,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/validasi-pimpinan-so/edit/:id",
        name: "Edit Validasi Pimpinan So",
        miniName: "EVPS",
        component: EditPimpinanSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/validas-pimpinan-so",
        name: "Validasi Pimpinan SO",
        miniName: "VPS",
        component: PimpinanSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/validasi-admin-so/edit/:id",
        name: "Edit Validasi Admin So",
        miniName: "EVAS",
        component: EditAdminApproveSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/validasi-admin-so",
        name: "Validasi Admin SO",
        miniName: "VAS",
        component: AdminApproveSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/validasi-penawaran-so/edit/:id",
        name: "Validasi Penawaran So",
        miniName: "VPS",
        component: EditValidasiSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/validasi-penawaran-so",
        name: "Validasi Penawaran So",
        miniName: "VPS",
        component: ValidasiSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/detail/:id",
        name: "Edit Sales Order",
        miniName: "ESO",
        component: EditSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/edit/:id",
        name: "Edit Sales Order",
        miniName: "ESO",
        component: EditSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/create",
        name: "Create Sales Order",
        miniName: "CSO",
        component: CreateSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/so-penawaran/detail/:id",
        name: "Edit Penawaran So",
        miniName: "CPP",
        component: DetailPenawaranSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/so-penawaran/edit/:id",
        name: "Edit Sales Order",
        miniName: "ESO",
        component: EditPenawaranSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order/so-penawaran/create",
        name: "Create Penawaran So",
        miniName: "CPP",
        component: CreatePenawaranSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/sales-order",
        name: "Sales Order",
        miniName: "SO",
        component: So,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/surat-jalan-so/cetak/:id",
        name: "Edit Penawaran So",
        miniName: "CPP",
        component: CetakSuratJalan,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/validasi-surat-jalan-so/edit/:id",
        name: "Edit Penawaran So",
        miniName: "CPP",
        component: ValidasiSuratJalanSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/surat-jalan-so/detail/:id",
        name: "Detail Penawaran So",
        miniName: "CPP",
        component: DetailSuratJalanSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/surat-jalan-so/edit/:id",
        name: "Edit Penawaran So",
        miniName: "CPP",
        component: EditSuratJalanSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/surat-jalan-so/create",
        name: "Create Penawaran So",
        miniName: "CPP",
        component: CreateSuratJalanSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/surat-jalan-so",
        name: "Surat Jalan",
        miniName: "SO",
        component: SuratJalanSo,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/invoice-so/cetak/:id",
        name: "Create Invoice So",
        miniName: "CPP",
        component: CetakInvoiceSO,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/validasi-direktur/edit/:id",
        name: "Create Invoice So",
        miniName: "CPP",
        component: ValidasiDirektur,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/validasi-invoice-so/edit/:id",
        name: "Create Invoice So",
        miniName: "CPP",
        component: Validasifinance,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/invoice-so/detail/:id",
        name: "Create Invoice So",
        miniName: "CPP",
        component: DetailInvoiceSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/invoice-so/edit/:id",
        name: "Create Invoice So",
        miniName: "CPP",
        component: EditInvoiceSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/invoice-so/create",
        name: "Create Invoice So",
        miniName: "CPP",
        component: CreateInvoiceSo,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/invoice-so",
        name: "Invoice",
        miniName: "SO",
        component: InvoiceSo,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-masuk/cetak/:id",
        name: "Validasi Direktur",
        miniName: "CPP",
        component: CetakBuktiKasMasuk,
        layout: "/cetak",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-masuk/validasi-direktur/validasi/:id",
        name: "Validasi Direktur",
        miniName: "CPP",
        component: ValidasiDirekturBkm,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-masuk/validasi-kepala-finance/validasi/:id",
        name: "Validasi kepala finance",
        miniName: "CPP",
        component: ValidasiAdminFinanceBkm,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-masuk/detail/:id",
        name: "Detail Bukti kas Masuk",
        miniName: "CPP",
        component: DetailBuktiKasMasuk,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-masuk/create",
        name: "Create bukti kas keluar",
        miniName: "CPP",
        component: CreateBuktiKasMasuk,
        layout: "/admin",
        hidden: true,
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/bukti-kas-masuk",
        name: "Bukti Kas Masuk",
        miniName: "BKM",
        component: BuktiKasMasuk,
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
      {
        path: "/so-return",
        name: "SO Retur",
        miniName: "BKM",
        component: "",
        layout: "/admin",
        roles: [
          "ROLE_SUPERADMIN",
          "ROLE_ADMIN",
          "ROLE_KARYAWAN",
          "ROLE_USER",
          "ROLE_OWNER",
        ],
      },
    ],
  },
];

export default routes;
